<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html> 
<html lang="en">
   <head>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="description" content="Neon Admin Panel" />
      <meta name="author" content="Laborator.co" />
      <link rel="icon" href="http://demo.neontheme.com/assets/images/favicon.ico">
      <title>NCC | Query Tables</title>
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css" id="style-resource-1">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/font-icons/entypo/css/entypo.css" id="style-resource-2">
      <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic" id="style-resource-3">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/bootstrap.css" id="style-resource-4">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/neon-core.css" id="style-resource-5">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/neon-theme.css" id="style-resource-6">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/neon-forms.css" id="style-resource-7">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/css/custom.css" id="style-resource-8">
      <script src="http://demo.neontheme.com/assets/js/jquery-1.11.3.min.js"></script> <!--[if lt IE 9]><script src="http://demo.neontheme.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]--> <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --> <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]--> <!-- TS1503071928: Neon - Responsive Admin Template created by Laborator --> 
   </head>
   <body class="page-body" >
      <!-- TS15030719284924: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
      <div class="page-container">
         <!-- TS150307192814033: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
         <div class="sidebar-menu">
            <div class="sidebar-menu-inner">
               <header class="logo-env">
                  <!-- logo --> 
                  <div class="logo"> <a href="http://demo.neontheme.com/dashboard/main/"> <font style="color:white; font-family: 'Times New Roman', Times, serif; font-style: oblique; font-size: 250%; font-weight: bold; ">NCC EDU</font> </a> </div>
                  <!-- logo collapse icon --> 
                  <div class="sidebar-collapse">
                     <a href="#" class="sidebar-collapse-icon">
                        <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> <i class="entypo-menu"></i> 
                     </a>
                  </div>
                  <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) --> 
                  <div class="sidebar-mobile-menu visible-xs">
                     <a href="#" class="with-animation">
                        <!-- add class "with-animation" to support animation --> <i class="entypo-menu"></i> 
                     </a>
                  </div>
               </header>
               <ul id="main-menu" class="main-menu">
                  <li> <a href="<?=base_url()?>"><i class="entypo-chart-bar"></i><span class="title">Query</span></a> </li>
               </ul>
            </div>
         </div>
         <div class="main-content">
            <!-- TS15030719284663: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
            <div class="row">
               <!-- Profile Info and Notifications --> 
               <div class="col-md-6 col-sm-8 clearfix">
                  <ul class="user-info pull-left pull-none-xsm">
                     <!-- Profile Info --> 
                     <li class="profile-info dropdown">
                        <!-- add class "pull-right" if you want to place this from right --> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="<?=base_url('files/11_EmmanuleDadzie.jpg')?>" alt="Emmanuel Kwabena Dadzie" class="img-circle" width="44" />
                        Emmanuel Dadzie Kwabena
                        </a> 
                        <ul class="dropdown-menu">
                           <!-- Reverse Caret --> 
                           <li class="caret"></li>
                           <!-- Profile sub-links --> 
                           <li> <a href="http://demo.neontheme.com/extra/timeline/"> <i class="entypo-user"></i>
                              Edit Profile
                              </a> 
                           </li>
                           <li> <a href="http://demo.neontheme.com/mailbox/main/"> <i class="entypo-mail"></i>
                              Inbox
                              </a> 
                           </li>
                           <li> <a href="http://demo.neontheme.com/extra/calendar/"> <i class="entypo-calendar"></i>
                              Calendar
                              </a> 
                           </li>
                           <li> <a href="#"> <i class="entypo-clipboard"></i>
                              Tasks
                              </a> 
                           </li>
                        </ul>
                     </li>
                  </ul>
                  <ul class="user-info pull-left pull-right-xs pull-none-xsm">
                     <!-- Raw Notifications --> 
                     <li class="notifications dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="entypo-attention"></i> <span class="badge badge-info">6</span> </a> 
                        <ul class="dropdown-menu">
                           <!-- TS150307192818542: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
                           <li class="top">
                              <p class="small"> <a href="#" class="pull-right">Mark all Read</a>
                                 You have <strong>3</strong> new notifications.
                              </p>
                           </li>
                           <li>
                              <ul class="dropdown-menu-list scroller">
                                 <li class="unread notification-success"> <a href="#"> <i class="entypo-user-add pull-right"></i> <span class="line"> <strong>New user registered</strong> </span> <span class="line small">
                                    30 seconds ago
                                    </span> </a> 
                                 </li>
                                 <li class="unread notification-secondary"> <a href="#"> <i class="entypo-heart pull-right"></i> <span class="line"> <strong>Someone special liked this</strong> </span> <span class="line small">
                                    2 minutes ago
                                    </span> </a> 
                                 </li>
                                 <li class="notification-primary"> <a href="#"> <i class="entypo-user pull-right"></i> <span class="line"> <strong>Privacy settings have been changed</strong> </span> <span class="line small">
                                    3 hours ago
                                    </span> </a> 
                                 </li>
                                 <li class="notification-danger"> <a href="#"> <i class="entypo-cancel-circled pull-right"></i> <span class="line">
                                    John cancelled the event
                                    </span> <span class="line small">
                                    9 hours ago
                                    </span> </a> 
                                 </li>
                                 <li class="notification-info"> <a href="#"> <i class="entypo-info pull-right"></i> <span class="line">
                                    The server is status is stable
                                    </span> <span class="line small">
                                    yesterday at 10:30am
                                    </span> </a> 
                                 </li>
                                 <li class="notification-warning"> <a href="#"> <i class="entypo-rss pull-right"></i> <span class="line">
                                    New comments waiting approval
                                    </span> <span class="line small">
                                    last week
                                    </span> </a> 
                                 </li>
                              </ul>
                           </li>
                           <li class="external"> <a href="#">View all notifications</a> </li>
                        </ul>
                     </li>
                     <!-- Message Notifications --> 
                     <li class="notifications dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="entypo-mail"></i> <span class="badge badge-secondary">10</span> </a> 
                        <ul class="dropdown-menu">
                           <!-- TS1503071928523: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
                           <li>
                              <form class="top-dropdown-search">
                                 <div class="form-group"> <input type="text" class="form-control" placeholder="Search anything..." name="s" /> </div>
                              </form>
                              <ul class="dropdown-menu-list scroller">
                                 <li class="active"> <a href="#"> <span class="image pull-right"> <img src="http://demo.neontheme.com/assets/images/thumb-1@2x.png" width="44" alt="" class="img-circle" /> </span> <span class="line"> <strong>Luc Chartier</strong>
                                    - yesterday
                                    </span> <span class="line desc small">
                                    This ain’t our first item, it is the best of the rest.
                                    </span> </a> 
                                 </li>
                                 <li class="active"> <a href="#"> <span class="image pull-right"> <img src="http://demo.neontheme.com/assets/images/thumb-2@2x.png" width="44" alt="" class="img-circle" /> </span> <span class="line"> <strong>Salma Nyberg</strong>
                                    - 2 days ago
                                    </span> <span class="line desc small">
                                    Oh he decisively impression attachment friendship so if everything. 
                                    </span> </a> 
                                 </li>
                                 <li> <a href="#"> <span class="image pull-right"> <img src="http://demo.neontheme.com/assets/images/thumb-3@2x.png" width="44" alt="" class="img-circle" /> </span> <span class="line">
                                    Hayden Cartwright
                                    - a week ago
                                    </span> <span class="line desc small">
                                    Whose her enjoy chief new young. Felicity if ye required likewise so doubtful.
                                    </span> </a> 
                                 </li>
                                 <li> <a href="#"> <span class="image pull-right"> <img src="http://demo.neontheme.com/assets/images/thumb-4@2x.png" width="44" alt="" class="img-circle" /> </span> <span class="line">
                                    Sandra Eberhardt
                                    - 16 days ago
                                    </span> <span class="line desc small">
                                    On so attention necessary at by provision otherwise existence direction.
                                    </span> </a> 
                                 </li>
                              </ul>
                           </li>
                           <li class="external"> <a href="http://demo.neontheme.com/mailbox/main/">All Messages</a> </li>
                        </ul>
                     </li>
                     <!-- Task Notifications --> 
                     <li class="notifications dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> <i class="entypo-list"></i> <span class="badge badge-warning">1</span> </a> 
                        <ul class="dropdown-menu">
                           <!-- TS1503071928491: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
                           <li class="top">
                              <p>You have 6 pending tasks</p>
                           </li>
                           <li>
                              <ul class="dropdown-menu-list scroller">
                                 <li> <a href="#"> <span class="task"> <span class="desc">Procurement</span> <span class="percent">27%</span> </span> <span class="progress"> <span style="width: 27%;" class="progress-bar progress-bar-success"> <span class="sr-only">27% Complete</span> </span> </span> </a> </li>
                                 <li> <a href="#"> <span class="task"> <span class="desc">App Development</span> <span class="percent">83%</span> </span> <span class="progress progress-striped"> <span style="width: 83%;" class="progress-bar progress-bar-danger"> <span class="sr-only">83% Complete</span> </span> </span> </a> </li>
                                 <li> <a href="#"> <span class="task"> <span class="desc">HTML Slicing</span> <span class="percent">91%</span> </span> <span class="progress"> <span style="width: 91%;" class="progress-bar progress-bar-success"> <span class="sr-only">91% Complete</span> </span> </span> </a> </li>
                                 <li> <a href="#"> <span class="task"> <span class="desc">Database Repair</span> <span class="percent">12%</span> </span> <span class="progress progress-striped"> <span style="width: 12%;" class="progress-bar progress-bar-warning"> <span class="sr-only">12% Complete</span> </span> </span> </a> </li>
                                 <li> <a href="#"> <span class="task"> <span class="desc">Backup Create Progress</span> <span class="percent">54%</span> </span> <span class="progress progress-striped"> <span style="width: 54%;" class="progress-bar progress-bar-info"> <span class="sr-only">54% Complete</span> </span> </span> </a> </li>
                                 <li> <a href="#"> <span class="task"> <span class="desc">Upgrade Progress</span> <span class="percent">17%</span> </span> <span class="progress progress-striped"> <span style="width: 17%;" class="progress-bar progress-bar-important"> <span class="sr-only">17% Complete</span> </span> </span> </a> </li>
                              </ul>
                           </li>
                           <li class="external"> <a href="#">See all tasks</a> </li>
                        </ul>
                     </li>
                  </ul>
               </div>
               <!-- Raw Links --> 
               <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                  <ul class="list-inline links-list pull-right">
                     <li class="dropdown language-selector">
                        Language: &nbsp;
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true"> <img src="http://demo.neontheme.com/assets/images/flags/flag-uk.png" width="16" height="16" /> </a> 
                        <ul class="dropdown-menu pull-right">
                           <li> <a href="#"> <img src="http://demo.neontheme.com/assets/images/flags/flag-de.png" width="16" height="16" /> <span>Deutsch</span> </a> </li>
                           <li class="active"> <a href="#"> <img src="http://demo.neontheme.com/assets/images/flags/flag-uk.png" width="16" height="16" /> <span>English</span> </a> </li>
                           <li> <a href="#"> <img src="http://demo.neontheme.com/assets/images/flags/flag-fr.png" width="16" height="16" /> <span>François</span> </a> </li>
                           <li> <a href="#"> <img src="http://demo.neontheme.com/assets/images/flags/flag-al.png" width="16" height="16" /> <span>Shqip</span> </a> </li>
                           <li> <a href="#"> <img src="http://demo.neontheme.com/assets/images/flags/flag-es.png" width="16" height="16" /> <span>Español</span> </a> </li>
                        </ul>
                     </li>
                     <li class="sep"></li>
                     <li> <a href="#" data-toggle="chat" data-collapse-sidebar="1"> <i class="entypo-chat"></i>
                        Tables
                        <span class="badge badge-success chat-notifications-badge is-hidden">0</span> </a> 
                     </li>
                     <li class="sep"></li>
                  </ul>
               </div>
            </div>
            <hr />
            <!-- TS150307192819517: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
            <ol class="breadcrumb bc-3" >
               <li> <a href="http://demo.neontheme.com/dashboard/main/"><i class="fa-home"></i>Home</a> </li>
               <li> <a href="http://demo.neontheme.com/tables/main/">Tables</a> </li>
               <li class="active"> <strong>Data Tables</strong> </li>
            </ol>
            <br /> <script type="text/javascript">
               jQuery( document ).ready( function( $ ) {
               var $table3 = jQuery("#table-3");
               var table3 = $table3.DataTable( {
					"aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
					dom: 'Bfrtip',
					buttons: [
					'copyHtml5',
					'excelHtml5',
					'csvHtml5',
					'pdfHtml5'
					]
               } );
               // Initalize Select Dropdown after DataTables is created
               $table3.closest( '.dataTables_wrapper' ).find( 'select' ).select2( {
               minimumResultsForSearch: -1
               });
               // Setup - add a text input to each footer cell
               $( '#table-3 tfoot th' ).each( function () {
               var title = $('#table-3 thead th').eq( $(this).index() ).text();
               $(this).html( '<input type="text" class="form-control" placeholder="Search ' + title + '" />' );
               } );
               // Apply the search
               table3.columns().every( function () {
               var that = this;
               $( 'input', this.footer() ).on( 'keyup change', function () {
               if ( that.search() !== this.value ) {
               that
               .search( this.value )
               .draw();
               }
               } );
               } );
               } );
            </script> 
            <?php $form_prop = array('id' => "query_form");?>
            <?=form_open(null , $form_prop)?>

                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="form-group"> 
                            <label class="control-label" for="about">Write your query</label> 
                            <textarea class="form-control autogrow" name="query" rows="5" placeholder="select * from table" style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 99px;"></textarea> 
                        </div> 
                    </div> 
                </div>
                <div class="row"> 
                    <div class="col-md-12"> 
                        <div class="form-group"> 
                            <button type="submit" class="btn btn-primary btn-block"> Query </button> 
                        </div> 
                    </div> 
                </div>
            <?=form_close()?>
            <h3>Loading Table with Column Filtering</h3>
            <table class="table table-bordered datatable" id="table-3">
                <thead>
                    <tr class="replace-inputs">
                        <?php
                            foreach ($results->result() as $key => $value) {
                                foreach ($value as $key_x => $value_x) {
                                    echo "<th> $key_x </th>";
                                }
                                break;
                            }
                        ?>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <?php
                            foreach ($results->result() as $key => $value) {
                                foreach ($value as $key_x => $value_x) {
                                    echo "<th> $key_x </th>";
                                }
                                break;
                            }
                        ?>
                    </tr>
                </tfoot>

                <tbody>
                    <tr>
                        <?php
                            foreach ($results->result() as $key => $value) {
                                foreach ($value as $key_x => $value_x) {
                                    echo "<th> $value_x </th>";
                                }
                            }
                        ?>
                </tbody>
                    
            </table>
            <br /> 
			<!-- Footer --> 
            <footer class="main">
               <div class="pull-right"> <a href="lustergh.com" target="_blank"><strong>LustterGH (Ncc student)</strong></a> </div>
               &copy; 2017 
            </footer>
         </div>
         <!-- TS150307192812561: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
         <div id="chat" class="fixed" data-current-user="Art Ramadani" data-order-by-status="1" data-max-chat-history="25">
            <div class="chat-inner">
               <h2 class="chat-header"> <a href="#" class="chat-close"><i class="entypo-cancel"></i></a> <i class="entypo-users"></i>
                  Table List
                  <span class="badge badge-success is-hidden">0</span> 
               </h2>
               <div class="chat-group" id="group-1"> 

                    <?php
                        foreach ($all_field_data as $key => $value) {
                            echo '<strong> '.$key.' </strong> ';
                                foreach ($value as $key_field_data => $value_field_data) {
                                    echo '<a href="#"><span class="user-status is-busy"></span> <em> '.$value_field_data->name.' </em> ('.$value_field_data->type.')</a>';

                                }
                        }
                    ?>
                </div>
            </div>
            <!-- conversation template --> 
            <div class="chat-conversation">
               <div class="conversation-header"> <a href="#" class="conversation-close"><i class="entypo-cancel"></i></a> <span class="user-status"></span> <span class="display-name"></span> <small></small> </div>
               <ul class="conversation-body"> </ul>
               <div class="chat-textarea"> <textarea class="form-control autogrow" placeholder="Type your message"></textarea> </div>
            </div>
         </div>
      </div>
      <!-- Imported styles on this page --> 
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/js/datatables/datatables.css" id="style-resource-1">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/js/select2/select2-bootstrap.css" id="style-resource-2">
      <link rel="stylesheet" href="http://demo.neontheme.com/assets/js/select2/select2.css" id="style-resource-3">
      <script src="http://demo.neontheme.com/assets/js/gsap/TweenMax.min.js" id="script-resource-1"></script> <script src="http://demo.neontheme.com/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js" id="script-resource-2"></script> <script src="http://demo.neontheme.com/assets/js/bootstrap.js" id="script-resource-3"></script> <script src="http://demo.neontheme.com/assets/js/joinable.js" id="script-resource-4"></script> <script src="http://demo.neontheme.com/assets/js/resizeable.js" id="script-resource-5"></script> <script src="http://demo.neontheme.com/assets/js/neon-api.js" id="script-resource-6"></script> <script src="http://demo.neontheme.com/assets/js/cookies.min.js" id="script-resource-7"></script> <script src="http://demo.neontheme.com/assets/js/datatables/datatables.js" id="script-resource-8"></script> <script src="http://demo.neontheme.com/assets/js/select2/select2.min.js" id="script-resource-9"></script> <script src="http://demo.neontheme.com/assets/js/neon-chat.js" id="script-resource-10"></script> <!-- JavaScripts initializations and stuff --> <script src="http://demo.neontheme.com/assets/js/neon-custom.js" id="script-resource-11"></script> <!-- Demo Settings --> <script src="http://demo.neontheme.com/assets/js/neon-demo.js" id="script-resource-12"></script> <script src="http://demo.neontheme.com/assets/js/neon-skins.js" id="script-resource-13"></script> <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', 'UA-28991003-7']);
         _gaq.push(['_setDomainName', 'demo.neontheme.com']);
         _gaq.push(['_trackPageview']);
         (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
      </script> 
   </body>
</html>